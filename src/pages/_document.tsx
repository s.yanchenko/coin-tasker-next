import { Head, Html, Main, NextScript } from 'next/document';
import { Theme } from '@radix-ui/themes';
import { ThemeProvider } from 'next-themes';

export default function Document() {
  return (
    <Html lang='en' suppressHydrationWarning>
      <Head />
      <body>
        <ThemeProvider attribute='class'>
          <Theme grayColor='mauve' panelBackground='solid' radius='full' accentColor='iris' appearance='dark'>
            <Main />
            <NextScript />
          </Theme>
        </ThemeProvider>
      </body>
    </Html>
  );
}
