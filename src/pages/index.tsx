import '@radix-ui/themes/styles.css';

import { Button, Flex, Heading } from '@radix-ui/themes';
import CashIcon from '@/assets/icons/cash.svg';
import Layout from '@/components/features/Layout';
import Card from '@/components/shared/Card';
import { TaskStatusEnum, TaskTypesEnum } from '@/constants/task';
import Tracker from '../components/shared/TaskTracker';

export default function Home() {
  return (
    <Layout>
      <Flex direction='column' gap='6'>
        <Flex justify='end'>
          <Button color='green'>
            {new Intl.NumberFormat().format(11200)}
            <CashIcon />
          </Button>
        </Flex>
        <Flex direction='column' gap='5'>
          <Heading>Текущая задача</Heading>
          <Tracker
            taskType={TaskTypesEnum.fitness}
            coinsAmount={1200}
            taskTitle='Сделать 10 приседаний'
            trackerDates={[
              new Date(2024, 0, 18, 23, 10, 0, 0),
              new Date(2024, 0, 18, 23, 20, 0, 0),
              new Date(2024, 0, 18, 23, 30, 0, 0),
              new Date(2024, 0, 18, 23, 50, 0, 0),
            ]}
            taskHref='/task'
            taskStatus={TaskStatusEnum.paused}
          />
        </Flex>
        <Flex direction='column' gap='5'>
          <Heading>Доступные задания</Heading>
          <Card />
        </Flex>
      </Flex>
    </Layout>
  );
}
