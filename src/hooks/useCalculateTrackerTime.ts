import { Dispatch, SetStateAction, useCallback, useEffect, useRef } from 'react';
import { getTrackerTime } from '@/utils/date/getTrackerTime';
import { TaskStatus } from '@/types/task';
import { TaskStatusEnum } from '@/constants/task';

export const useCalculateTrackerTime = ({
  setStartTime,
  trackerDates,
  taskStatus,
}: {
  setStartTime: Dispatch<SetStateAction<number>>;
  trackerDates: Date[];
  taskStatus: TaskStatus;
}) => {
  const trackerRef = useRef<ReturnType<typeof setInterval>>();

  const startTimer = useCallback(() => {
    if (trackerRef.current) {
      clearInterval(trackerRef.current);
    }

    trackerRef.current = setInterval(() => {
      setStartTime(prev => (prev || 0) + 1000);
    }, 1000);
  }, [setStartTime]);

  useEffect(() => {
    setStartTime(getTrackerTime(trackerDates));

    if (taskStatus === TaskStatusEnum.paused) {
      return;
    }

    startTimer();

    return () => {
      clearInterval(trackerRef.current);
    };

    // eslint-disable-next-line
  }, [taskStatus]);

  const stopTimer = useCallback(() => {
    clearInterval(trackerRef.current);
  }, []);

  return { stopTimer, startTimer };
};
