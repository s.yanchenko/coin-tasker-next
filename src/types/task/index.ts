import { TaskStatusEnum, TaskTypesEnum } from '@/constants/task';

export type TaskType = `${TaskTypesEnum}`;

export type TaskStatus = `${TaskStatusEnum}`;
