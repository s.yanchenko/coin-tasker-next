export const moneyFormat = (amount: number) => new Intl.NumberFormat().format(amount);
