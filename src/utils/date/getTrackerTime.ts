export const getTrackerTime = (trackerDates: Date[]) => {
  const currentDate = new Date();

  return trackerDates.reduce((previousValue, currentValue, currentIndex) => {
    if (currentIndex === 0 && trackerDates.length === 1) {
      return previousValue + Math.abs(currentDate.getTime() - currentValue.getTime());
    }

    if (currentIndex === 0) {
      return previousValue;
    }

    return previousValue + Math.abs(currentValue.getTime() - trackerDates[currentIndex - 1].getTime());
  }, 0);
};
