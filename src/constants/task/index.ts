export enum TaskTypesEnum {
  book = 'book',
  fitness = 'fitness',
  video = 'video',
  telegram = 'telegram',
  film = 'film',
}

export enum TaskStatusEnum {
  done = 'done',
  paused = 'paused',
  progress = 'progress',
}
