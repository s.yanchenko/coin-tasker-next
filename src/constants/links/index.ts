export const PAGE_LINKS = {
  HOME: '/',
  CURRENT_TASK: '/current-task',
  PROFILE: '/profile',
};
