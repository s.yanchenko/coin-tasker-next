import React, { useState } from 'react';
import { TaskStatus, TaskType } from '@/types/task';
import CustomIcon from '@/components/shared/CustomIcon';
import classNames from 'classnames';
import { Badge, Strong, Text } from '@radix-ui/themes';
import { moneyFormat } from '@/utils/transform/moneyFormat';
import Link from 'next/link';
import { useCalculateTrackerTime } from '@/hooks/useCalculateTrackerTime';
import { TaskStatusEnum } from '@/constants/task';
import { msToTimeString } from '@/utils/date/msToTimeString';
import cn from './styles.module.scss';

type TaskTrackerProps = {
  taskType: TaskType;
  taskTitle: string;
  coinsAmount: number;
  trackerDates: Date[];
  taskHref: string;
  taskStatus: TaskStatus;
};

export default function TaskTracker({
  taskType,
  taskTitle,
  coinsAmount,
  trackerDates,
  taskHref,
  taskStatus,
}: TaskTrackerProps) {
  const [startTime, setStartTime] = useState<number>(0);

  const { stopTimer, startTimer } = useCalculateTrackerTime({
    setStartTime,
    trackerDates,
    taskStatus,
  });

  const handleClickControl = () => {
    if (taskStatus === TaskStatusEnum.progress) {
      stopTimer();
    }
    if (taskStatus === TaskStatusEnum.paused) {
      startTimer();
    }
  };

  return (
    <Link href={taskHref}>
      <div className={cn.container}>
        <div className={classNames(cn.icon_circle, cn[`icon_circle_${taskType}`])}>
          <CustomIcon name={taskType} />
        </div>
        <div className={cn.meta_container}>
          <div className={cn.meta}>
            <Text>
              <Strong>{taskTitle}</Strong>
            </Text>
            <Badge color='green'>
              {moneyFormat(coinsAmount)}
              <CustomIcon name='cash' />
            </Badge>
          </div>
          <div
            className={cn.meta_controls}
            onClick={e => {
              e.preventDefault();
              handleClickControl();
            }}
          >
            <Text color='gray'>{msToTimeString(startTime)}</Text>
            <button className={cn.meta_controls_button}>
              <CustomIcon name={taskStatus === TaskStatusEnum.paused ? 'play' : 'pause'} />
            </button>
          </div>
        </div>
      </div>
    </Link>
  );
}
