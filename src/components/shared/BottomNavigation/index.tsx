import React from 'react';
import classNames from 'classnames';
import HomeIcon from '@/assets/icons/home.svg';
import ClipboardIcon from '@/assets/icons/clipboard-list.svg';
import UserIcon from '@/assets/icons/user.svg';
import Link from 'next/link';
import { PAGE_LINKS } from '@/constants/links';
import { useRouter } from 'next/router';
import cn from './styles.module.scss';

export default function BottomNavigation() {
  const router = useRouter();

  return (
    <div className={cn.wrapper}>
      <div className={cn.container}>
        <Link href={PAGE_LINKS.HOME} className={cn.link}>
          <HomeIcon
            className={classNames(cn.icon, cn.nav_element, {
              [cn.nav_element_active]: router.pathname === PAGE_LINKS.HOME,
            })}
          />
        </Link>
        <Link href={PAGE_LINKS.CURRENT_TASK} className={cn.link}>
          <ClipboardIcon
            className={classNames(cn.icon, cn.nav_element, {
              [cn.nav_element_active]: router.pathname === PAGE_LINKS.CURRENT_TASK,
            })}
          />
        </Link>
        <Link href={PAGE_LINKS.PROFILE} className={cn.link}>
          <UserIcon
            className={classNames(cn.icon, cn.nav_element, {
              [cn.nav_element_active]: router.pathname === PAGE_LINKS.PROFILE,
            })}
          />
        </Link>
      </div>
    </div>
  );
}
