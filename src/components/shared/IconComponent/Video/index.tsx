import VideoIcon from '@/assets/icons/video.svg';

export default function Video() {
  return <VideoIcon />;
}
