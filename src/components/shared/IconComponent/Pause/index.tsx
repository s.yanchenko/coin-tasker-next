import PauseIcon from '@/assets/icons/pause.svg';

export default function Pause() {
  return <PauseIcon />;
}
