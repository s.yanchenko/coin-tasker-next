import TelegramIcon from '@/assets/icons/telegram.svg';

export default function Telegram() {
  return <TelegramIcon />;
}
