import BookIcon from '@/assets/icons/book-open.svg';

export default function Book() {
  return <BookIcon />;
}
