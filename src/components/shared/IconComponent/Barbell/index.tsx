import BarbellIcon from '@/assets/icons/barbell.svg';

export default function Barbell() {
  return <BarbellIcon />;
}
