import PlayIcon from '@/assets/icons/play.svg';

export default function Play() {
  return <PlayIcon />;
}
