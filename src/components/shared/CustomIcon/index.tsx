import React from 'react';
import dynamic from 'next/dynamic';
import { TaskTypesEnum } from '@/constants/task';

const DynamicBarbell = dynamic(() => import('@/components/shared/IconComponent/Barbell'));

const DynamicBook = dynamic(() => import('@/components/shared/IconComponent/Book'));

const DynamicCash = dynamic(() => import('@/components/shared/IconComponent/Cash'));

const DynamicFilm = dynamic(() => import('@/components/shared/IconComponent/Film'));

const DynamicPause = dynamic(() => import('@/components/shared/IconComponent/Pause'));

const DynamicPlay = dynamic(() => import('@/components/shared/IconComponent/Play'));

const DynamicTelegram = dynamic(() => import('@/components/shared/IconComponent/Telegram'));

const DynamicVideo = dynamic(() => import('@/components/shared/IconComponent/Video'));

export default function CustomIcon({ name }: { name: string }) {
  switch (name) {
    case TaskTypesEnum.fitness:
      return <DynamicBarbell />;
    case TaskTypesEnum.book:
      return <DynamicBook />;
    case TaskTypesEnum.film:
      return <DynamicFilm />;
    case TaskTypesEnum.telegram:
      return <DynamicTelegram />;
    case TaskTypesEnum.video:
      return <DynamicVideo />;
    case 'cash':
      return <DynamicCash />;
    case 'pause':
      return <DynamicPause />;
    case 'play':
      return <DynamicPlay />;
    default:
      return null;
  }
}
