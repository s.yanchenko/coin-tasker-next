import React, { ReactNode } from 'react';
import BottomNavigation from '@/components/shared/BottomNavigation';
import cn from './styles.module.scss';

export default function Layout({ children }: { children: ReactNode }) {
  return (
    <div className={cn.wrapper}>
      <div className={cn.page_wrapper}>{children}</div>
      <BottomNavigation />
    </div>
  );
}
